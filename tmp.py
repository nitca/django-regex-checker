def counter(primer):
    znaks = ['+', '-', '/', '*']
    mark = None
    for znak in znaks:
        if znak in primer:
            mark = znak
            break

    if mark is None:
        return False, 'Неверный пример'
    else:
        primer = primer.split(znak)
        a = int(primer[0])
        b = int(primer[1])
        if znak == '+':
            primer = str(a + b)
        elif znak == '-':
            primer = str(a - b)
        if znak == '/':
            if b == 0:
                return False, 'На ноль делить нельзя'
            primer = str(a / b)
        if znak == '*':
            primer = str(a * b)

        return True, primer
