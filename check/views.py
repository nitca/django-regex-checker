from django.shortcuts import render
from .forms import GetRegexForm
from .check_regex import check_regex


def index(request):
    if request.method == "POST":
        form = GetRegexForm(request.POST)
        if form.is_valid():
            print(form.cleaned_data)
            regex = form.cleaned_data['get_regex']
            text = form.cleaned_data['get_text']
            if check_regex(regex, text):
                answer = 'Регулярное выражение верно!'
            else:
                answer = 'Регулярное выражение ошибочно!'
        else:
            answer = 'Ошибка!'
    else:
        form = GetRegexForm()
        answer = ''
    context = {
        'title': 'Проверка регулярных выражений',
        'form': form,
        'answer': answer,
    }

    return render(request, 'check/index.html', context=context)
