from django import forms


class GetRegexForm(forms.Form):
    get_regex = forms.CharField(label='Регулярное выражение:')
    get_text = forms.CharField(label='Проверяемый текст:')
